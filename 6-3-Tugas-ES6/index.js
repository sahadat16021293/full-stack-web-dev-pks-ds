// arrow function
// const myFunction = (nama,tinggal) => {
//   console.log('Hai Namaku' + nama + " Q tinggal di"+tinggal)
// }

// // panggil Function
// myFunction('nama','jakarta')


function multiply(a, b = 1) {
  return a * b;
}
 
console.log(multiply(5, 2));
// expected output: 10
 
console.log(multiply(5));
// expected output: 5 