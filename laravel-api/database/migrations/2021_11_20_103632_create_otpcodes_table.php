<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtpcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otpcodes', function (Blueprint $table) {
            $table->uuid('id')->primary();
            // field otp di buat uniqeu
            $table->integer('otp')->unique();
            // field untuk menseting kode opt berlaku selama lima menit
            $table->timestamp('valid_until');
            $table->uuid('user_id');
            // menambah foreign 
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('otpcodes');
    }
}
