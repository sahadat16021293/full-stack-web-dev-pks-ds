<?php

namespace App;


use App\roles;
use illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Authenticatable;

class users extends Authenticatable implements JWTSubject
{
    use Notifiable;
    // definisikan dulu field di table users
    protected $fillable = ['id', 'username', 'email', 'name', 'role_id', 'password', 'email_verified_at'];
    protected $keyType = 'string';
    protected $primarykey = 'id';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            // jika modelnya kosong

            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }

            $model->role_id = roles::where('name', 'author')->first()->id;
        });
    }

    // setting relasi
    // di sin ada belongto karena di table users ada foreign key dari table roles
    public function roles()
    {
        return $this->belongsTo('App\roles');
    }



    public function Otp_Code()
    {
        return $this->hasOne('App\otpcode');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
