<?php

namespace App\Http\Controllers\Auth;

use App\users;
use App\otpcode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RegenerateOtpController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allreques = $request->all();
        $validator = Validator::make($allreques, [
            'email'   => 'required',

        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        // ambil data di table users yang emailnya = dengan email yang di inputkan 
        $users = users::where('email', $request->email)->first();

        // jika otp nya ada 
        if ($users->otp_code) {

            // maka hapus code sebelumnya
            $users->otp_code->delete();
        }

        // proses membuat otp code 
        do {
            // proses generae kode opt
            $random = mt_rand(100000, 999999);
            // cek apakah di table otp sudah ada otpnya
            $cek = otpcode::where('otp', $random)->first();
        } while ($cek);

        $now = Carbon::now();
        $otp_code = otpcode::create([
            // menyiapkan dan menyiapkan data untu di simpan ke table users
            // membuat otp code random

            'otp' => $random,

            // setting kode otp agar berlaku hanya 5 menit
            'valid_until' => $now->addMinute(5),

            // ambil user id dari table users
            'user_id' => $users->id
        ]);

        // kirim email otp ke email register


        // membuat notifikasi sementara dengan format json
        return response()->Json([
            'success' => 'true',
            'message' => 'OTP code berhasi di generate',
            'data' => [
                'user' => $users,
                'otp_code' => $otp_code
            ]
        ]);
    }
}
