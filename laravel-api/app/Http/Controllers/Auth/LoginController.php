<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        dd('masuk ke halama login');
        $allreques = $request->all();
        $validator = Validator::make($allreques, [
            'email'   => 'required',
            'password'   => 'required'

        ]);
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json([
                'Success' => 'false',
                'Message' => 'Email atau password tidak di temukan'
            ], 401);
        }
        return response()->json([
            'Success' => 'true',
            'Message' => 'User Berhasil Login',
            'data' => [
                'user' => auth()->user(),
                'token' => $token
            ]
        ], 401);
    }
}
