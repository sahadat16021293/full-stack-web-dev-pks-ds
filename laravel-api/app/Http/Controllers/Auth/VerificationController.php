<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\otpcode;
use App\users;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class verificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // dd('masuk sini');
        $allreques = $request->all();
        $validator = Validator::make($allreques, [
            'otp'   => 'required',

        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        // seleksi/tambil  data di table users yang emailnya = dengan email yang di inputkan 
        $otp_code = otpcode::where('otp', $request->otp)->first();

        // periksa apakah kode otp di temukan atau tidak
        // jika code otp tidak di temukan
        // jalankan kode di bawah ini 
        if (!$otp_code) {
            return response()->json([
                'success' => false,
                'message' => 'Kode OTP tidak di temukan'
            ], 400);
        }

        // periksa detail hari sekarang
        $now = Carbon::now();

        // periksa apakah kode otp masih berlaku apa tidak 
        // jika detail sekarnag lebih besar dari otp kode yang didapat
        if ($now > $otp_code->valid_until) {
            return response()->json([
                'success' => false,
                'message' => 'Kode OTP sudah tidak berlaku'
            ], 400);
        }

        $user = users::find($otp_code->user_id);
        $user->update([
            'email_verified_at' => $now
        ]);

        $otp_code->delete();

        return response()->json([
            'success' => true,
            'message' => 'user berhasi di verifikasi',
            'data' => $user
        ], 200);
    }
}
