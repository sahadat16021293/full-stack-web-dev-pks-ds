<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        // dd di bawah ini untuk mengetas apakah data yang di butuhkansudah masuk ke contoller atau belum


        // dd('masuk ke update password');


        $allreques = $request->all();
        $validator = Validator::make($allreques, [
            'email'   => 'required',
            // password harus di isi minimal 6 karakter
            'password'   => 'required|confirmed|min:6',

        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        // seleksi table users yang emailnya = yang di inputkan
        $users = Users::where('email', $request->email)->first();

        if (!$users) {
            return response()->json([
                'success' => false,
                'message' => 'Email tidak di temukan'
            ], 400);
        }
        // jika email berhasil di temukan langsung jalankan kode di bawah ini
        $users->update([

            'password' => Hash::make($request->newPassword)
        ]);
        return response()->json([
            'success' => true,
            'message' => 'Password BerhasiL Di Ubah',
            'data' => $users

        ], 200);
    }
}
