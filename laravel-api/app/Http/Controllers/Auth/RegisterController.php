<?php

namespace App\Http\Controllers\Auth;


use App\post;
use App\users;
use App\otpcode;
use Carbon\Carbon;
use PHPUnit\Util\Json;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        dd('ini halaman register');
        $allreques = $request->all();
        $validator = Validator::make($allreques, [
            'name'   => 'required',
            // email yang dimasukkan haru uniqe berdasarkan di table users pada kolom email dan harus benar benar email yang di masukkan 
            'email' => 'required|unique:users,email|email',
            // username ini unique ke table users  pada kolom user name di database
            'username'   => 'required|unique:users,username',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //success save to database user

        $users = users::create($allreques);

        // proses membuat otp code 
        do {
            // proses generae kode opt
            $random = mt_rand(100000, 999999);
            // cek apakah di table otp sudah ada otpnya
            $check = otpcode::where('otp', $random)->first();
        } while ($check);

        // tracking waktu sekarang berdasarkan jam di jakarta
        $now = Carbon::now();
        $otp_code = otpcode::create([
            // menyiapkan dan menyiapkan data untu di simpan ke table users
            // membuat otp code random

            'otp' => $random,

            // setting kode otp agar berlaku hanya 5 menit
            'valid_until' => $now->addMinute(5),

            // ambil user id dari table users
            'user_id' => $users->id
        ]);

        // kirim email otp ke email register


        // membuat notifikasi sementara dengan format json
        return response()->Json([
            'success' => 'true',
            'message' => 'data user berhasil di tambahkan ',
            'data' => [
                'user' => $users,
                'otp_code' => $otp_code
            ]
        ]);
    }
}
