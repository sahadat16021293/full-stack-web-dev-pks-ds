<?php

namespace App\Http\Controllers;

use App\roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $roles = roles::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Semua List Data Roles',
            'data'    => $roles
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set validation
        $allreques = $request->all();
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $roles = roles::create([
            'name'     => $request->name,

        ]);

        //success save to database
        if ($roles) {

            return response()->json([
                'success' => true,
                'message' => 'roles Berhasil di buat',
                'data'    => $roles
            ], 201);
        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'roles Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find roles$roles by ID
        $roles = roles::findOrfail($id);

        if ($id) {
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Data roles',
                'data'    => $roles
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'data denga id :' . $id . ' tida di temukan',

        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required'

        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $roles = roles::findOrFail($id);

        if ($roles) {

            //update $roles
            $roles->update([
                'name'     => $request->name

            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data berhasil di Updated',
                'data'    => $roles
            ], 200);
        }

        //data $roles not found
        return response()->json([
            'success' => false,
            'message' => 'data Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find roles by ID
        $roles = roles::findOrfail($id);

        if ($roles) {

            //delete roles
            $roles->delete();

            return response()->json([
                'success' => true,
                'message' => 'roles Deleted',
            ], 200);
        }

        //data roles not found
        return response()->json([
            'success' => false,
            'message' => 'roles Not Found',
        ], 404);
    }
}
