<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\comments;

class CommentsController extends Controller
{
    public function index()
    {
        //get data from table posts
        $comments = comments::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data comments',
            'data'    => $comments
        ], 200);
    }
    /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find comments by ID
        $comments = comments::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data comments',
            'data'    => $comments
        ], 200);
    }
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $comments = comments::create([
            'content'     => $request->content,
            'post_id'   => $request->post_id
        ]);

        //success save to database
        if ($comments) {

            return response()->json([
                'success' => true,
                'message' => 'comments Created',
                'data'    => $comments
            ], 201);
        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'comments Failed to Save',
        ], 409);
    }
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $comments
     * @return void
     */
    public function update(Request $request, comments $comments)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find comments by ID
        $comments = comments::findOrFail($comments->id);

        if ($comments) {

            //update comments
            $comments->update([
                'content'     => $request->content,
                'post_id'   => $request->post_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'comments Updated',
                'data'    => $comments
            ], 200);
        }

        //data comments not found
        return response()->json([
            'success' => false,
            'message' => 'comments Not Found',
        ], 404);
    }
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find comments by ID
        $comments = comments::findOrfail($id);

        if ($comments) {

            //delete comments
            $comments->delete();

            return response()->json([
                'success' => true,
                'message' => 'comments Deleted',
            ], 200);
        }

        //data comments not found
        return response()->json([
            'success' => false,
            'message' => 'comments Not Found',
        ], 404);
    }
}
