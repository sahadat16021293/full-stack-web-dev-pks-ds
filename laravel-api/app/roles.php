<?php

namespace App;

use illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class roles extends Model
{
    protected $fillable = ['name'];
    protected $primarykey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            // jika modelnya kosong
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    // atur relasi
    public function user()
    {
        // di ini ada has many ceritanya terjadi relasi one to many antara table users table role
        // dan di table roles menjadi primary key di table  users ada foreign key dari table roles ini



        // catatan penting dan harus di ingat
        // dan jangan sampai lupa
        // relasi ini di baca table roles ini memiliki banyak users
        return $this->hasMany('App\user');
    }
}
