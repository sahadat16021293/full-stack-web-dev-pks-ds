<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;


class otpcode extends Model
{
    protected $fillable = ['otp', 'user_id', 'valid_until'];
    protected $primarykey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            // jika modelnya kosong
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
    public function users()
    {
        return $this->belongsTo('App\users');
    }
}
