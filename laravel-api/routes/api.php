<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// // Route Original
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::apiResource('/post', 'PostController');
Route::apiResource('/comment', 'CommentsController');
Route::apiResource('/role', 'RolesController');


// route api 

// kenapa kok di grup
// karena controller sama sama ada di folder
Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth'
], function () {
    Route::post('register', 'RegisterController')->name('auth.register');
    Route::post('regeneret-otp-code', 'RegenerateOtpController')->name('auth.regeneret-otp-code');
    Route::post('verifikasi', 'verificationController')->name('auth.verifikasi');
    Route::post('update-password', 'UpdatePasswordController')->name('auth.update_password');
    Route::post('login', 'LoginController')->name('auth.login');
});


// tes api
// Route::get('/test', function () {
//     return 'masuk pak eko';
// })->middleware('auth:api');
